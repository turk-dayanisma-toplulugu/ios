//
// Created by Maarten Billemont on 2020-09-13.
// Copyright (c) 2020 Lyndir. All rights reserved.
//

import UIKit

class WTFLabel: UILabel {
    override var isHidden: Bool {
        get {
            super.isHidden
        }
        set {
            super.isHidden = newValue
        }
    }
}
