//
// Created by Maarten Billemont on 2020-01-02.
// Copyright (c) 2020 Lyndir. All rights reserved.
//

import Foundation

// printf <secret> | openssl enc -[ed] -aes-128-cbc -a -A -K <appSecret> -iv 0
let appSecret    = ""
let appSalt      = ""
let mpwSalt      = ""
let sentryDSN    = ""
let countlyKey   = ""
let countlySalt  = ""
let freshchatApp = ""
let freshchatKey = ""
